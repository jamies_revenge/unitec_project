using UnityEngine;
using System.Collections;

public class PlaneBehaviour : MonoBehaviour
{
    public GameObject planePrefab;
    public GameObject rightJoystickPrefab;
    public GameObject leftJoystickPrefab;

    public float rotateSpeed;
    public float speed;

    public bool floating;               // Is the plane floating in water?

    private Joystick leftJoystick;          // Joystick Scripts
    private Joystick rightJoystick;

	// Initialization
	void Start()
    {
        leftJoystick = leftJoystickPrefab.GetComponent<Joystick>();
        rightJoystick = rightJoystickPrefab.GetComponent<Joystick>();

        floating = false;
	}
	
	// Update is called once per frame
	void Update()
    {
        if (planePrefab.transform.localPosition.y <= 0)
        {
            floating = true;

            planePrefab.transform.localPosition = new Vector3(
                planePrefab.transform.localPosition.x, 0, planePrefab.transform.localPosition.z);
        }

        // Joystick Controls
        float rotateAmount = rotateSpeed * Time.deltaTime;
        float thrustAmount = speed * Time.deltaTime;

        if (rightJoystick.position.x < 0)
        {
            //rigidbody.AddForce(-5, 0, 0, ForceMode.Acceleration);
            planePrefab.transform.Rotate(0, -rotateAmount, -rotateAmount);
        }
        else if (rightJoystick.position.x > 0)
        {
            //rigidbody.AddForce(5, 0, 0, ForceMode.Acceleration);
            planePrefab.transform.Rotate(0, rotateAmount, rotateAmount);
        }

        if (leftJoystick.position.y < 0)
        {
            planePrefab.transform.Rotate(rotateAmount, 0, 0);
        }
        else if (leftJoystick.position.y > 0)
        {
            planePrefab.transform.Rotate(-rotateAmount, 0, 0);
        }
	}
}
