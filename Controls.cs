using UnityEngine;
using System.Collections;
using System;

public class Controls : MonoBehaviour {

    // Public Variables
    public float sliderValue = 0.0f;
    public float minValue;
    public float maxValue;
    public GUIStyle sliderThumb;
    public GameObject planePrefab;
    

    // Private variables
    private PlaneBehaviour planeScript;
    private bool started = false;


    // Initialisation
    void Start()
    {
        GameEventManager.GameStart += GameStart;
        GameEventManager.GameOver += GameOver;

        planeScript = planePrefab.GetComponent<PlaneBehaviour>();
        planeScript.speed = sliderValue;        
    }


    private void OnGUI()
    {
        if (started)
        {
            GUI.skin.verticalSliderThumb = sliderThumb;
            sliderThumb.fixedHeight = Screen.height / 12;
            sliderThumb.fixedWidth = Screen.width / 12;
            sliderValue = GUI.VerticalSlider(
                new Rect(9, Screen.height / 2.5f, sliderThumb.fixedWidth, Screen.height / 4),
                sliderValue, maxValue, minValue);

            if (planeScript.speed < sliderValue)
            {
                planeScript.started = true;
                planeScript.speed += sliderValue / 10 * Time.deltaTime;
            }
            else if (planeScript.speed > sliderValue)
            {
                planeScript.speed -= sliderValue / 8 * Time.deltaTime;
            }
        }
    }


    private void GameStart()
    {
        started = true;
    }


    private void GameOver()
    {
        started = false;
    }
}
