using UnityEngine;
using System.Collections;

public class GUImanager : MonoBehaviour {

    // Public variables
    public GUIText timer;
    public GUIText speedIndicator;
    public static float seconds;
    public static float spd;


    // Private variables
    private bool started;
    private bool gameOver;


	// Initialization
	void Start()
    {
        GameEventManager.GameStart += GameStart;
        GameEventManager.GameOver += GameOver;

        started = false;
        seconds = 3.4999f;
	}
	

	// Update is called once per frame
	void Update()
    {
        seconds -= Time.deltaTime;
	}


    // GUI rendering
    void OnGUI()
    {
        if (!gameOver)
        {
            if (started)
            {
                //Speed Gauge
                speedIndicator.text = (spd.ToString("#0") + "km/h");

                timer.text = seconds.ToString("#0");
            }
            else
            {
                speedIndicator.text = "";

                if (seconds <= 0.5f)
                {
                    GameEventManager.TriggerGameStart();
                }
                else
                {
                    timer.text = seconds.ToString("#0");
                }
            }

            if (seconds <= 0)
            {
                GameEventManager.TriggerGameOver();
            }
        }
        else
        {
            //GUI.Box(new Rect(Screen.width / 4, Screen.height / 2.175f, Screen.width / 2, Screen.height / 3), "");

            if (GUI.Button(new Rect(Screen.width / 4 - 10, Screen.height / 2 - Screen.height / 8, Screen.width / 4, Screen.height / 4), "TRY AGAIN"))
            {

            }

            if (GUI.Button(new Rect(Screen.width / 2 + 10, Screen.height / 2 - Screen.height / 8, Screen.width / 4, Screen.height / 4), "MAIN MENU"))
            {
                Application.LoadLevel("MainMenu");
            }
        }
    }


    private void GameStart()
    {
        seconds = 59.4999f;
        //seconds = 3;
        timer.text = seconds.ToString("#0");
        started = true;
        gameOver = false;
    }
    

    private void GameOver()
    {
        gameOver = true;
        started = false;
        timer.text = "GAME OVER";
    }
}
