using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    public GUISkin Story;
    public GUISkin Mini;
    public GUISkin Info;

    private void OnGUI()
    {
        GUI.skin = Story;
        if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 175, 200, 100), "Story"))
        {
            Application.LoadLevel("Story");
        }

        GUI.skin = Mini;
        if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 50, 200, 100), "Mini"))
        {
            Application.LoadLevel("Mini");
        }

        GUI.skin = Info;
        if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 + 75, 200, 100), "Info"))
        {
            Application.LoadLevel("Info"); 
        }
    }
}
